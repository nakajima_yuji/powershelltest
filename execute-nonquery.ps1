Add-Type -Path '.\Oracle.ManagedDataAccess.dll'

$connStr = 'User Id=hr;Password=hr;Data Source=localhost/XE'

function Execute-NonQueryInternal {
  param(
    [Parameter(Mandatory)]
    [string]$sqlStm,
    [Parameter(Mandatory)]
    [Oracle.ManagedDataAccess.Client.OracleConnection]$conn
  )

  function Dispose-SQLCommandQuietly([Oracle.ManagedDataAccess.Client.OracleCommand]$comm) {
    if ($comm -eq $null) { return }
    $comm.Dispose()

    trap { continue }
  }
  
  try {
    $comm = $conn.CreateCommand()
    $comm.ExecuteNonQuery($sqlStm)
  }
  finally {
    Dispose-SQLCommandQuietly($comm)
  }
}

function Execute-NonQuery {
  param(
    [Parameter(Mandatory, ValueFromPipeline)]
    [string]$sqlStm,
    [Parameter(Mandatory)]
    [string]$connStr
  )

  function Close-SQLConnectionQuietly([Oracle.ManagedDataAccess.Client.OracleConnection]$conn) {
    if ($conn -eq $null) { return }
    if ($conn.State -eq 'Open') { $conn.Close() }

    trap { continue }
  }

  begin {
    $conn = New-Object Oracle.ManagedDataAccess.Client.OracleConnection($connStr)
    $conn.Open()

    trap {
      Close-SQLConnectionQuietly($conn)
      break
    }
  }

  process {
    Execute-NonQueryInternal -sqlStm $sqlStm -conn $conn

    trap {
      Close-SQLConnectionQuietly($conn)
      break
    }
  }

  end {
    Close-SQLConnectionQuietly($conn)
  }
}
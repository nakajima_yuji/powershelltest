function Filter-A {
  param(
    [Parameter(Mandatory, Position = 0, ValueFromPipeline)]
    [string] $str
  )

  begin {
    echo "begin A"
  }

  process {
    $str + "A"
  }

  end {
    echo "end A"
  }
}

function Filter-B {
  param(
    [Parameter(Mandatory, Position = 0, ValueFromPipeline)]
    [string] $str
  )

  begin {
    echo "begin B"
  }

  process {
    $str + "B"
  }

  end {
    echo "end B"
  }
}

1..5 | Filter-A | Filter-B
Add-Type -Path ".\Oracle.ManagedDataAccess.dll"

$connStr = "User Id=hr;Password=hr;Data Source=localhost/XE"

function Read-Data([Oracle.ManagedDataAccess.Client.OracleDataReader]$reader) {
  if ($reader.HasRow) {
    while($reader.Read()) {
      $items = New-Object System.Object[] $reader.FieldCount
      $count = $reader.GetOracleValues($items)
      $items
    }
  }
}

function Execute-DataReaderInternalWithCommand {
  param(
    [Parameter(Mandatory)]
    [string]$sqlStm,
    [Parameter(Mandatory)]
    [Oracle.ManagedDataAccess.Client.OracleCommand]$comm
  )

  function Close-DataReaderQuietly([Oracle.ManagedDataAccess.Client.OracleDataReader]$reader) {
    if ($reader -eq $null) { return }
    if ($reader.IsClosed) { return }
    $reader.Close()

    trap { continue }
  }

  try {
    $reader = $comm.ExecuteReader($sqlStm)
    Read-Data $reader
  }
  finally {
    Close-DataReaderQuietly($reader)
  }
}

function Execute-DataReaderInternal {
  param(
    [Parameter(Mandatory)]
    [string]$sqlStm,
    [Parameter(Mandatory)]
    [Oracle.ManagedDataAccess.Client.OracleConnection]$conn
  )

  function Dispose-SQLCommandQuietly([Oracle.ManagedDataAccess.Client.OracleCommand]$comm) {
    if ($comm -eq $null) { return }
    $comm.Dispose()

    trap { continue }
  }

  try {
    $comm = $conn.CreateCommand()
    Execute-DataReaderInternalWithCommand -sqlStm $sqlStm -comm $comm
  }
  finally {
    Dispose-SQLCommandQuietly($comm)
  }
}

function Execute-DataReader {
  param(
    [Parameter(Mandatory, ValueFromPipeline)]
    [string]$sqlStm,
    [Parameter(Mandatory)]
    [string]$connStr
  )

  function Close-SQLConnectionQuietly([Oracle.ManagedDataAccess.Client.OracleConnection]$conn) {
    if ($conn -eq $null) { return }
    if ($conn.State -eq 'Open') { $conn.Close() }

    trap { continue }
  }

  begin {
    $conn = New-Object Oracle.ManagedDataAccess.Client.OracleConnection($connStr)
    $conn.Open()

    trap {
      Close-SQLConnectionQuietly($conn)
      break
    }
  }

  process {
    Execute-DataReaderInternal -sqlStm $sqlStm -conn $conn

    trap {
      Close-SQLConnectionQuietly($conn)
      break
    }
  }

  end {
    Close-SQLConnectionQuietly($conn)
  }
}